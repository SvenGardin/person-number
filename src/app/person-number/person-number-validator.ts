import { AbstractControl } from '@angular/forms';
import * as moment from 'moment';

export function personNumberValidator(control: AbstractControl): { [key: string]: string } | null {
    let errorName: string;
    const personNumber = control.value;

    if (personNumber == null) {
        errorName = null;
    } else if (personNumber.substr(6, 1) === 'T') {
        errorName = validateTemporaryPersonNumber(personNumber);
    } else {
        errorName = validateRealPerson(personNumber);
    }

    if (errorName != null) {
        return { customerror: errorName };
    }
    return null;
}

function validateRealPerson(personNumber: string): string {
    let errorName: string;

    errorName = validateNumeric(personNumber);
    if (errorName != null) {
        return errorName;
    }

    errorName = validateDatePart(personNumber);
    if (errorName != null) {
        return errorName;
    }

    errorName = validateChecksum(personNumber);
    if (errorName != null) {
        return errorName;
    }
    return errorName;
}

function validateTemporaryPersonNumber(personNumber: string): string {
    let errorName: string;

    errorName = validateNumeric(personNumber.substr(0, 6));
    if (errorName != null) {
        return errorName;
    }

    errorName = validateDatePart(personNumber);
    if (errorName != null) {
        return errorName;
    }

    errorName = validateNumeric(personNumber.substr(7, 3));
    if (errorName != null) {
        return errorName;
    }

    return errorName;
}

function validateNumeric(inputString: string): string {
    if (isNaN(Number(inputString))) {
        return 'nonNumeric';
    }
    return null;
}

function validateDatePart(personNumber: string): string {
    if (personNumber.length < 6) {
        return null;
    }
    const valid = moment(personNumber.substring(0, 6), ['YYMMDD']).isValid();
    if (valid) {
        return null;
    }
    return 'invalidDate';
}

function validateChecksum(personNumber): string {
    if (personNumber.length !== 10) {
        return null;
    }

    let checkNumber = 2;
    let computedChecksum = 0;
    const existingChecksum = Math.trunc(personNumber.substr(9, 1));

    for (let i = 0; i < 9; i++) {
        let number = Math.trunc(personNumber.charAt(i));
        number = number * checkNumber;
        computedChecksum += Math.trunc(number / 10);
        computedChecksum += Math.trunc(number % 10);
        checkNumber = checkNumber === 2 ? 1 : 2;
    }
    computedChecksum = computedChecksum % 10;
    if (computedChecksum > 0) {
        computedChecksum = 10 - computedChecksum;
    }
    if (existingChecksum === computedChecksum) {
        return null;
    }
    return 'invalidChecksum';
}

