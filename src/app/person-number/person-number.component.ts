import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { personNumberValidator } from './person-number-validator';

@Component({
  selector: 'app-person-number',
  templateUrl: './person-number.component.html',
  styleUrls: ['./person-number.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: PersonNumberComponent,
      multi: true
    }
  ]
})

export class PersonNumberComponent implements OnInit, ControlValueAccessor {

  personNumberControl: FormControl;
  value = '';
  private onChange: any;

  get personNumber() {
    return this.personNumberControl.value;
  }

  get invalidLength(): boolean {
    const errors = this.personNumberControl.errors;

    if (errors == null || (errors.minlength == null && errors.maxlength == null)) {
      return false;
    }
    return true;
  }

  get nonNumeric() {
    return this.isCustomError('nonNumeric');
  }

  get invalidDate() {
    return this.isCustomError('invalidDate');
  }

  get invalidChecksum() {
    return this.isCustomError('invalidChecksum');
  }

  constructor() {
    this.createForm();
  }

  ngOnInit(): void {
    this.onChanges();
  }

  writeValue(obj: any): void {
    if (obj) {
      this.value = obj;
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState?(isDisabled: boolean): void {
  }

  private onChanges() {
    this.personNumberControl.valueChanges.subscribe(val => {
      if (this.personNumberControl.valid) {
        this.value = val;
      } else {
        this.value = '';
      }
      this.onChange(this.value);
    });
  }

  private createForm() {
    this.personNumberControl = new FormControl(null,
      [Validators.maxLength(10),
      Validators.minLength(10),
        personNumberValidator]);
  }

  private isCustomError(customErrorName: string) {
    const errors = this.personNumberControl.errors;

    if (errors == null || errors.customerror == null || errors.customerror !== customErrorName) {
      return false;
    }
    return true;
  }

}
