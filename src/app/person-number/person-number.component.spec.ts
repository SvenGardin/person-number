import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonNumberComponent } from './person-number.component';
import { BrowserModule } from '@angular/platform-browser';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule, FormControl } from '@angular/forms';
import { personNumberValidator } from './person-number-validator';

describe('PersonNumberValidator', () => {
  let control: FormControl;

  beforeEach(() => {
    control = new FormControl();
  });

  it('should pass on regular person number', () => {
    checkForPass(control, '1807205461');
  });

  it('should indicate checksum error ', () => {
    checkForErrorName(control, '1807205462', '{"customerror":"invalidChecksum"}');
  });

  it('should pass on temporary person number', () => {
    checkForPass(control, '180729T123');
  });

  it('should indicate invalid date on regular person number', () => {
    checkForErrorName(control, '1807391234', '{"customerror":"invalidDate"}');
  });

  it('should indicate invalid date on temporary person number', () => {
    checkForErrorName(control, '180739T123', '{"customerror":"invalidDate"}');
  });

  it('should indicate non numeric on regular person number', () => {
    checkForErrorName(control, '180729123q', '{"customerror":"nonNumeric"}');
  });

  it('should indicate non numeric on temporary person number', () => {
    checkForErrorName(control, '180729T12q', '{"customerror":"nonNumeric"}');
  });

  it('should indicate non numeric in date part on temporary person number', () => {
    checkForErrorName(control, '18071qT123', '{"customerror":"nonNumeric"}');
  });

  it('should pass numeric string less than 6 characters', () => {
    checkForPass(control, '12345');
  });
});

function checkForPass(control: FormControl, personNumber: string) {
  control.setValue(personNumber);
  const result = personNumberValidator(control);
  expect(result).toBeNull();
}

function checkForErrorName(control: FormControl, personNumber: string, errorName: string) {
  control.setValue(personNumber);
  const result = JSON.stringify(personNumberValidator(control));
  expect(result).toBe(errorName);
}

describe('PersonNumberController', () => {
  let control: PersonNumberComponent;

  beforeEach(() => {
    control = new PersonNumberComponent();
    control.personNumberControl = new FormControl();
  });

  it('should get min incorrect Length error', () => {
    control.personNumberControl.setErrors({
      minlength: true
    });
    expect(control.invalidLength).toBeTruthy();
  });

  it('should get nonNumeric false on minlength', () => {
    control.personNumberControl.setErrors({
      minlength: true
    });
    expect(control.nonNumeric).toBeFalsy();
  });

  it('should get nonNumeric false on invalidDate', () => {
    control.personNumberControl.setErrors({
      customerror: 'invalidDate'
    });
    expect(control.nonNumeric).toBeFalsy();
  });

  it('should get nonNumeric true ', () => {
    control.personNumberControl.setErrors({
      customerror: 'nonNumeric'
    });
    expect(control.nonNumeric).toBeTruthy();
  });

  it('should get invalidChecksum true ', () => {
    control.personNumberControl.setErrors({
      customerror: 'invalidChecksum'
    });
    expect(control.invalidChecksum).toBeTruthy();
  });

  it('should get max incorrectLength error', () => {
    control.personNumberControl.setErrors({
      maxlength: true
    });
    expect(control.invalidLength).toBeTruthy();
  });

  it('should get the person number ', () => {
    const personNumber = '1807191234';
    control.personNumberControl.setValue(personNumber);
    expect(control.personNumber).toBe(personNumber);
  });
});

describe('PersonNumberComponent', () => {
  let component: PersonNumberComponent;
  let fixture: ComponentFixture<PersonNumberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserModule,
        MatInputModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        FormsModule
      ],
      declarations: [PersonNumberComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonNumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

