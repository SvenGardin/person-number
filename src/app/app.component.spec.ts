import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { PersonNumberComponent } from './person-number/person-number.component';

describe('AppComponent', () => {

  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserModule,
        MatInputModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        FormsModule
      ],
      declarations: [AppComponent, PersonNumberComponent]
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
  }));

  it('should create the app', async(() => {
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'person-number'`, async(() => {
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('person-number');
  }));

  it('should render title in a h1 tag', async(() => {
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to person-number!');
  }));

  it('should deliver person numer on valid user input ', () => {
    fixture.detectChanges();
    const personNumber = '1807232408';
    const hostElement = fixture.nativeElement;
    const personNumberInput: HTMLInputElement = hostElement.querySelector('input.mat-input-element.mat-form-field-autofill-control');
    personNumberInput.value = personNumber;
    personNumberInput.dispatchEvent(new Event('input'));

    expect(fixture.componentInstance.personNumberControl.value).toEqual(personNumber);
  });

  it('should deliver empty string on invalid user input ', () => {
    fixture.detectChanges();
    const personNumber = '1807332408';
    const hostElement = fixture.nativeElement;
    const personNumberInput: HTMLInputElement = hostElement.querySelector('input.mat-input-element.mat-form-field-autofill-control');
    personNumberInput.value = personNumber;
    personNumberInput.dispatchEvent(new Event('input'));

    expect(fixture.componentInstance.personNumberControl.value).toEqual('');
  });
});
