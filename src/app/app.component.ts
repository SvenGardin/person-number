import { Component } from '@angular/core';
import { FormControl, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'person-number';
  seasons =  ['Winter', 'Spring', 'Summer', 'Autumn'];

  exampleForm = this.fb.group({
    personNumberControl: [''],
    favoriteSeason: ['']
  });


  constructor(private fb: FormBuilder) { }
}
