import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatRadioModule, MatRadioGroup, MatRadioButton } from '@angular/material/radio';

import { AppComponent } from './app.component';
import { PersonNumberComponent } from './person-number/person-number.component';

@NgModule({
  declarations: [
    AppComponent,
    PersonNumberComponent
  ],
  imports: [
    BrowserModule,
    MatInputModule,
    MatRadioModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
